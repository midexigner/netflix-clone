import React, { useEffect, useState } from 'react'
import axios from '../../services/axios';
import requests from '../../services/requests'
import './Banner.css'

const base_url = "https://image.tmdb.org/t/p/original/"

const Banner = () => {
    const [movie, setMovies] = useState([]);

     useEffect(()=>{
        async function fetchData() {
            const request = await axios.get(requests.fetchNetflixOriginals);
            setMovies(request.data.results[Math.floor(Math.random()* request.data.results.length)]);
            return request;
            }
            
             fetchData();
       
    },[]) 
   
    const truncate = (str,n)=>{
        return str?.length >n ?str.substr(0,n - 1) + "..." :str;
    }

    return (
        <header className="banner" 
        style={{
            backgroundSize:"Cover",
            backgroundImage:`url(${base_url}${movie?.backdrop_path})`,
            backgroundPosition:"center center"
    }}>
           
            {/* Background image */}
            <div className="banner__contents">
        {/* title */}
        <h1 className="banner__title">{movie?.title || movie?.name || movie?.original_name }</h1>
        {/* div > 2 buttons */}
        <div className="banner__buttons">
            <button className="banner__button">Play</button>
            <button className="banner__button">My List</button>
        </div>
        {/* description */}
        <h1 className="banner__description">{truncate(movie?.overview,150)}</h1>
        </div>
        <div className="banner--fadeBottom"></div>
        </header>
    )
}

export default Banner
